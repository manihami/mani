package ir.khodadad.contact.ui.insert;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ir.khodadad.contact.R;
import ir.khodadad.contact.data.local.db.AppDatabase;
import ir.khodadad.contact.data.local.db.model.ContactModel;
import ir.khodadad.contact.data.repository.ContactRepo;

public class InsertActivity extends AppCompatActivity {

    EditText name, phone, address, email;
    FloatingActionButton add;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        AppDatabase database = AppDatabase.getInstance(this);
        ContactRepo repository = new ContactRepo(database);

        name = findViewById(R.id.insert_name);
        phone = findViewById(R.id.insert_number);
        address = findViewById(R.id.insert_address);
        email = findViewById(R.id.insert_email);

        add = findViewById(R.id.insert_add);

        add.setOnClickListener(v -> {

            if(!name.getText().toString().isEmpty() && !phone.getText().toString().isEmpty()) {
                ContactModel model = new ContactModel(0, name.getText().toString(),
                        phone.getText().toString(), address.getText().toString(),
                        email.getText().toString(), "Mobile");

                repository.insertContact(model).subscribe();
                finish();
            }
        });
    }
}