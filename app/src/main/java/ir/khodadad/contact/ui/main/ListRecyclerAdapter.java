package ir.khodadad.contact.ui.main;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.khodadad.contact.R;
import ir.khodadad.contact.data.local.db.model.ContactModel;
import ir.khodadad.contact.ui.contact.ContactActivity;
import ir.khodadad.contact.ui.insert.InsertActivity;

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListRecyclerAdapter.ViewHolder1> {

    private Context context;
    private List<ContactModel> list;

    public ListRecyclerAdapter(Context context, List<ContactModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);
        return new ViewHolder1(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListRecyclerAdapter.ViewHolder1 holder, int position) {

        ContactModel model = list.get(position);

        holder.name.setText(model.getName());
        holder.phone.setText(model.getNumber());


        holder.cardHolder.setOnClickListener(v -> {
            context.startActivity(new Intent(context, ContactActivity.class).putExtra("position", position));
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder{

        TextView name, phone;
        CardView cardHolder;

        public ViewHolder1(View itemView) {
            super(itemView);

            cardHolder = itemView.findViewById(R.id.item_holder);
            name = itemView.findViewById(R.id.item_name);
            phone = itemView.findViewById(R.id.item_phone);
        }
    }

}
