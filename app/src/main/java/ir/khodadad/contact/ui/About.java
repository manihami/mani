package ir.khodadad.contact.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import ir.khodadad.contact.R;
import ir.khodadad.contact.ui.main.MainActivity;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button btn2 ;
        setContentView(R.layout.activity_about);
        btn2= findViewById(R.id.btn2);
        btn2.setOnClickListener(v -> {
            Intent in =new Intent(this, MainActivity.class);
            startActivity(in); });

    }
}