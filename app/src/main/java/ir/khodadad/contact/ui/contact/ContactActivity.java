package ir.khodadad.contact.ui.contact;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import io.reactivex.disposables.CompositeDisposable;
import ir.khodadad.contact.R;
import ir.khodadad.contact.data.local.db.AppDatabase;
import ir.khodadad.contact.data.local.db.model.ContactModel;
import ir.khodadad.contact.data.repository.ContactRepo;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class ContactActivity extends AppCompatActivity {

    ImageView call, message, delete;
    TextView name, phone, address, email;
    TextView tv1, tv2;
    int position;
    ContactRepo repository;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        AppDatabase database = AppDatabase.getInstance(this);
        repository = new ContactRepo(database);

        position = getIntent().getExtras().getInt("position");

        call = findViewById(R.id.contact_call);
        message = findViewById(R.id.contact_message);
        delete = findViewById(R.id.contact_delete);

        name = findViewById(R.id.contact_name);
        phone = findViewById(R.id.contact_phone);
        address = findViewById(R.id.contact_address);
        email = findViewById(R.id.contact_email);

        tv1 = findViewById(R.id.textview2);
        tv2 = findViewById(R.id.textview3);



        CompositeDisposable disposable = new CompositeDisposable();

        disposable.add(repository.getListData().subscribe(list -> {

            name.setText(list.get(position).getName());
            phone.setText(list.get(position).getNumber());

            if (list.get(position).getAddress().isEmpty()) {
                address.setVisibility(GONE);
                tv1.setVisibility(GONE);
            } else {
                address.setVisibility(VISIBLE);
                tv1.setVisibility(VISIBLE);
                address.setText(list.get(position).getAddress());
            }

            if (list.get(position).getEmail().isEmpty()) {
                email.setVisibility(INVISIBLE);
                tv2.setVisibility(GONE);
            } else {
                email.setVisibility(VISIBLE);
                tv2.setVisibility(VISIBLE);
                email.setText(list.get(position).getAddress());
            }

            call.setOnClickListener(v -> {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel: " + list.get(position).getNumber())));
            });

            message.setOnClickListener(v -> {

                Uri uriSms = Uri.parse("smsto:" + list.get(position).getNumber());
                Intent intentSMS = new Intent(Intent.ACTION_SENDTO, uriSms);
                intentSMS.putExtra("sms_body", "Hello");
                startActivity(intentSMS);

            });

            delete.setOnClickListener(v -> {

                disposable.clear();
                showDialog(this, list.get(position));

            });
        }));
    }

    private void showDialog(Context context, ContactModel model){

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        TextView cancel = dialog.findViewById(R.id.dialog_delete_cancel);
        cancel.setOnClickListener(v -> dialog.dismiss());

        TextView delete = dialog.findViewById(R.id.dialog_delete_button);
        delete.setOnClickListener(v -> {
            repository.deleteContact(model).subscribe();
            finish();
        });

        dialog.show();

    }
}