package ir.khodadad.contact.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ir.khodadad.contact.R;
import ir.khodadad.contact.ui.main.MainActivity;

public class Blind extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blind);

        Button btn,btn1 ;
        btn= findViewById(R.id.btn);

        btn.setOnClickListener(v -> {
            Intent in =new Intent(this,About.class);
            startActivity(in); });
        btn1= findViewById(R.id.btn1);
        btn1.setOnClickListener(v -> {
            Intent in =new Intent(this, MainActivity.class);
            startActivity(in); });
    }
}