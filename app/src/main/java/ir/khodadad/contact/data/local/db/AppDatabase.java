package ir.khodadad.contact.data.local.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import ir.khodadad.contact.data.local.db.dao.ContactDao;
import ir.khodadad.contact.data.local.db.model.ContactModel;

@Database(entities = {ContactModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context, AppDatabase.class, "Data-base").build();
        }
        return instance;
    }

    public abstract ContactDao contactDao();

}