package ir.khodadad.contact.data.local.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import ir.khodadad.contact.data.local.db.model.ContactModel;

@Dao
public interface ContactDao {

    @Insert
    void insertData(ContactModel model);

    @Query("SELECT * FROM Contact")
    Flowable<List<ContactModel>> getData();

    @Update
    void updateData(ContactModel model);

    @Delete
    void deleteData(ContactModel model);

}